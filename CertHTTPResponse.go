package main

import (
	"bytes"
	"encoding/pem"
	"fmt"
	"log"
	"net/http"
	"time"
)

var (
	CertPEM, CertDER []byte
	protocol         = "https"
	hostname         = "localhost"
)

func init() {
	CertPEM = []byte(`-----BEGIN CERTIFICATE-----
MIIG2DCCBcCgAwIBAgIMHU5RBQqhIiFaN/m9MA0GCSqGSIb3DQEBCwUAMHsxCzAJ
BgNVBAYTAkRFMRswGQYDVQQIDBJCYWRlbi1XdWVydHRlbWJlcmcxEjAQBgNVBAcM
CUthcmxzcnVoZTEqMCgGA1UECgwhS2FybHNydWhlIEluc3RpdHV0ZSBvZiBUZWNo
bm9sb2d5MQ8wDQYDVQQDDAZLSVQtQ0EwHhcNMTcwNTMxMTQzMDE0WhcNMjAwODI3
MTQzMDE0WjCBjzELMAkGA1UEBhMCREUxGzAZBgNVBAgMEkJhZGVuLVd1ZXJ0dGVt
YmVyZzESMBAGA1UEBwwJS2FybHNydWhlMSowKAYDVQQKDCFLYXJsc3J1aGUgSW5z
dGl0dXRlIG9mIFRlY2hub2xvZ3kxIzAhBgNVBAMMGnNjY3NlYy1zaXJpb3MuY2Vy
dC5raXQuZWR1MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtBOUohBk
VElbE831klrJmMPRI5PwyxI7WkrKlb9Wq/8QwZ+fgHo5wYxak6QT7qx/KhqmGBKD
0XlXy6n2swzh7leXQ0b6pWdaJ6r/k650I2YO95neddB3jzWCC71QD85ucfQSw22f
r36ySn/AdlVwr2nX8pOqqG6UPdvDV/zr/WgIl6bFsLQTppWBChhaiyNwNHw+lD6K
zIwgHAahtm6kGEN38wsGgG/9Pp9p328wFbMgao86Wl3hOs4wMuTExWGX3ip5GmI/
+d8LcAGZczfpavVHf9daXBbjTUrvYNOWgQnAAP7mydxZnlOLgSPaHVHD0izKOxuI
Ye3WOZka7DVvJzpEz8yON5YVUUQuVteRGKiPhA6v2I+/fH5NujnY8schSaESBOEr
XDWMyHitvqp9kmZdDc4G3RkcF1v+xYFxNFOx8vdvI7zvHEKOR7SqPtHITWBAYra1
kkgxXBi/mL0kKCIzIaH32/g2s1y0ckfP+qo4w9bZODvRRcC7wuuO92gTIjG/yPCT
c5ukzBRC1wt6NpvmoZNTiDcdcSuB7BqtG6kufvJhwdWCgIu009x5pWbBaPsNeqjR
0yhJPMA26QOf0ReGZYTw9aZfsTuaGcwyb1S5uRNsx2CHZTFF7ZDP/iybUKpgmeBS
I8Vd3tsnT4HX8PTSPfE3QnPBlLT8rhVosvkCAwEAAaOCAkUwggJBMAkGA1UdEwQC
MAAwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMB0GA1UdDgQW
BBRDIWMTtkIRk/T/JECQxUDAH0HExTAfBgNVHSMEGDAWgBQEGr8ck5E909k9sN4T
I+WacPQuCDAlBgNVHREEHjAcghpzY2NzZWMtc2lyaW9zLmNlcnQua2l0LmVkdTB9
BgNVHR8EdjB0MDigNqA0hjJodHRwOi8vY2RwMS5wY2EuZGZuLmRlL2tpdC1jYS1n
Mi9wdWIvY3JsL2NhY3JsLmNybDA4oDagNIYyaHR0cDovL2NkcDIucGNhLmRmbi5k
ZS9raXQtY2EtZzIvcHViL2NybC9jYWNybC5jcmwwgc0GCCsGAQUFBwEBBIHAMIG9
MDMGCCsGAQUFBzABhidodHRwOi8vb2NzcC5wY2EuZGZuLmRlL09DU1AtU2VydmVy
L09DU1AwQgYIKwYBBQUHMAKGNmh0dHA6Ly9jZHAxLnBjYS5kZm4uZGUva2l0LWNh
LWcyL3B1Yi9jYWNlcnQvY2FjZXJ0LmNydDBCBggrBgEFBQcwAoY2aHR0cDovL2Nk
cDIucGNhLmRmbi5kZS9raXQtY2EtZzIvcHViL2NhY2VydC9jYWNlcnQuY3J0MFkG
A1UdIARSMFAwEQYPKwYBBAGBrSGCLAEBBAMFMBEGDysGAQQBga0hgiwCAQQDATAP
Bg0rBgEEAYGtIYIsAQEEMA0GCysGAQQBga0hgiweMAgGBmeBDAECAjANBgkqhkiG
9w0BAQsFAAOCAQEAVZvaeNpe66Eo6DSFGr2G7T3XSY6o4YOSWlyG0EtszUrxYGfE
MIdLn05w94bemQqVQMRG+Zc33d2Pl2QOagUMexmnUNdTHQaJbht5Sxtehui/v+yy
WoCuZM6rZDzZR9DJNc9ofJpuIEDptkj4hP6GdaNYmip3H/KSBe4LLUB/JwSscJuf
bTen9Pm8xqXQTaovxch4q9ITwYYZg/FUrCBCFe07xg06ekuka7ztO6xYDRaLJDIe
9fceG0w+t/vqmzdyKvot0CkRZRPaEzl1PKGc4N3pbl9f+4By2ZA8+HGfZy/9Tgaf
7lIqamVGcPKD4U24AvrELnWTGW1BfWhitC5b+g==
-----END CERTIFICATE-----
`)
	block, _ := pem.Decode([]byte(CertPEM))
	CertDER = block.Bytes
}

type test struct {
	Path    string
	Name    string
	Payload []byte
	Headers [][2]string
}

func main() {
	var (
		index = new(bytes.Buffer)
		tests = []test{
			{
				"/pem/no_mimetype",
				"PEM: no mime type",
				CertPEM,
				nil,
			},
			{
				"/pem/x-pem-file",
				"PEM: application/x-pem-file",
				CertPEM,
				[][2]string{{"Content-Type", "application/x-pem-file"}},
			},
			{
				"/pem/x-x509-email-cert",
				"PEM: application/x-x509-email-cert",
				CertPEM,
				[][2]string{{"Content-Type", "application/x-x509-email-cert"}},
			},
			{
				"/der/no_mimetype",
				"DER: no mime type",
				CertDER,
				nil,
			},
			{
				"/der/pkix-cert",
				"DER: application/pkix-cert",
				CertDER,
				[][2]string{{"Content-Type:", "application/pkix-cert"}},
			},
			{
				"/der/x-x509-ca-cert",
				"DER: application/x-x509-ca-cert",
				CertDER,
				[][2]string{{"Content-Type:", "application/x-x509-ca-cert"}},
			},
			{
				"/der/x-x509-user-cert",
				"DER: application/x-x509-user-cert",
				CertDER,
				[][2]string{{"Content-Type:", "application/x-x509-user-cert"}},
			},
			{
				"/pem/no_mimetype_attachment",
				"PEM: no mime type (attachment)",
				CertPEM,
				[][2]string{{"Content-Disposition", "attachment; filename=no_mimetype_attachment.pem"}},
			},
			{
				"/pem/x-pem-file_attachment",
				"PEM: application/x-pem-file (attachment)",
				CertPEM,
				[][2]string{{"Content-Type", "application/x-pem-file"}, {"Content-Disposition", "attachment; filename=x-pem-file_attachment.pem"}},
			},
			{
				"/pem/x-x509-email-cert_attachment",
				"PEM: application/x-x509-email-cert (attachment)",
				CertPEM,
				[][2]string{{"Content-Type", "application/x-x509-email-cert"}, {"Content-Disposition", "attachment; filename=x-x509-email-cert_attachment_attachment.pem"}},
			},
			{
				"/der/no_mimetype_attachment",
				"DER: no mime type (attachment)",
				CertDER,
				[][2]string{{"Content-Disposition", "attachment; filename=.der"}},
			},
			{
				"/der/pkix-cert_attachment",
				"DER: application/pkix-cert (attachment)",
				CertDER,
				[][2]string{{"Content-Type:", "application/pkix-cert"}, {"Content-Disposition", "attachment; filename=pkix-cert.der"}},
			},
			{
				"/der/x-x509-ca-cert_attachment",
				"DER: application/x-x509-ca-cert (attachment)",
				CertDER,
				[][2]string{{"Content-Type:", "application/x-x509-ca-cert"}, {"Content-Disposition", "attachment; filename=x-x509-ca-cert_attachment.der"}},
			},
			{
				"/der/x-x509-user-cert_attachment",
				"DER: application/x-x509-user-cert (attachment)",
				CertDER,
				[][2]string{{"Content-Type:", "application/x-x509-user-cert"}, {"Content-Disposition", "attachment; filename=x-x509-user-cert_attachment.der"}},
			},
		}
	)

	fmt.Fprintln(index, "<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<ul>")

	for _, t := range tests {
		handler := GenHandler(t)
		http.HandleFunc(t.Path, handler)
		fmt.Fprintf(index, "  <li><a href=\"%s\">%s</a></li>\n", t.Path, t.Name)
	}

	fmt.Fprintln(index, "</ul>\n</body>\n</html>")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(index.Bytes())
	})

	s := &http.Server{
		Addr:         ":8443",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	log.Fatal(s.ListenAndServe())
	//log.Fatal(s.ListenAndServeTLS("server.cert", "server.key"))
}

func GenHandler(t test) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		for _, h := range t.Headers {
			w.Header().Set(h[0], h[1])
		}
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Write(t.Payload)
	}
}
